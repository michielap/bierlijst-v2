<?php

//Deel voor gebruikersbeheer
function gebruiker_toevoegen($gebruiker_naam) {
	global $pdo;

	//Voeg de gebruiker toe aan gebruikers tabel
	$stmt = $pdo->prepare("INSERT INTO gebruikers (gebruiker_naam, actief) VALUES (:gebruiker_naam, 1)");
	$stmt->bindParam(':gebruiker_naam', $gebruiker_naam);
	$stmt->execute();

	//Haal gebruiker_id op van de nieuwe gebruiker
	$stmt = $pdo->prepare("SELECT LAST_INSERT_ID()");
	$stmt->execute();
	$gebruiker_id = $stmt->fetch();
	$gebruiker_id = $gebruiker_id[0];

	//Zet de flessen stand van de nieuwe gebruiker op nul
	$stmt = $pdo->prepare("INSERT INTO stand_fles (gebruiker_id, aantal_fles) VALUES (:gebruiker_id, 0)");
	$stmt->bindParam(':gebruiker_id', $gebruiker_id);
	$stmt->execute();

	//Zet de kratten stand van de nieuwe gebruiker op nul
	$stmt = $pdo->prepare("INSERT INTO stand_krat (gebruiker_id, aantal_krat) VALUES (:gebruiker_id, 0)");
	$stmt->bindParam(':gebruiker_id', $gebruiker_id);
	$stmt->execute();
}

function gebruiker_verwijderen($gebruiker_id) {
	global $pdo;

	$stmt = $pdo->prepare("UPDATE gebruikers SET actief = 0 WHERE gebruiker_id = :gebruiker_id");
	$stmt->bindParam(':gebruiker_id', $gebruiker_id);
	$stmt->execute();
}

function gebruikers_ophalen() {
	global $pdo;
	$stmt = $pdo->prepare("SELECT * FROM gebruikers WHERE actief = 1");
	$stmt->execute();
	$gebruikers = $stmt->fetchAll(PDO::FETCH_ASSOC);
	return $gebruikers;
}

function gebruiker_naam_ophalen($gebruiker_id){
	global $pdo;
	$stmt = $pdo->prepare("SELECT gebruiker_naam FROM gebruikers WHERE gebruiker_id = :gebruiker_id");
	$stmt->bindParam(':gebruiker_id', $gebruiker_id);
	$stmt->execute();
	$gebruiker_naam = $stmt->fetch();
	$gebruiker_naam = $gebruiker_naam[0];
	return $gebruiker_naam;
}



//Deel voor bier en krat mutaties
function aantal_fles_ophalen(){
	global $pdo;
	$stmt = $pdo->prepare("SELECT * FROM stand_fles WHERE gebruiker_id IN (SELECT gebruiker_id FROM gebruikers WHERE actief = 1)");
	$stmt->execute();
	$stand_fles = $stmt->fetchAll(PDO::FETCH_ASSOC);

	foreach ($stand_fles as $key => $row)
	{
	    $aantal[$key] = $row['aantal_fles'];
	}
	array_multisort($aantal, SORT_DESC, $stand_fles);

	return $stand_fles;
}

function aantal_krat_ophalen($gebruiker_id){
	global $pdo;
	$stmt = $pdo->prepare("SELECT aantal_krat FROM stand_krat WHERE gebruiker_id = :gebruiker_id");
	$stmt->bindParam(':gebruiker_id', $gebruiker_id);
	$stmt->execute();
	$stand_krat = $stmt->fetch();
	return $stand_krat[0];
}

function mutatie_fles($gebruiker_id, $mutatie_fles_aantal){
	global $pdo;

	//Voeg een mutatie toe aan de mutatie_fles tabel
	$stmt = $pdo->prepare("INSERT INTO mutatie_fles (gebruiker_id, mutatie) VALUES (:gebruiker_id, :mutatie_fles_aantal)");
	$stmt->bindParam(':gebruiker_id', $gebruiker_id);
	$stmt->bindParam(':mutatie_fles_aantal', $mutatie_fles_aantal);
	$stmt->execute();

	//Haal de huidige flessen stand op van de gebruiker
	$stmt = $pdo->prepare("SELECT aantal_fles FROM stand_fles WHERE gebruiker_id = :gebruiker_id");
	$stmt->bindParam(':gebruiker_id', $gebruiker_id);
	$stmt->execute();
	$huidige_stand = $stmt->fetch();
	$huidige_stand = $huidige_stand[0];

	//Schrijf de nieuwe flessen stand weg
	$nieuwe_stand = $huidige_stand + $mutatie_fles_aantal;
	$stmt = $pdo->prepare("UPDATE stand_fles SET aantal_fles = :nieuwe_stand WHERE gebruiker_id = :gebruiker_id");
	$stmt->bindParam(':nieuwe_stand', $nieuwe_stand);
	$stmt->bindParam(':gebruiker_id', $gebruiker_id);
	$stmt->execute();
}

function mutatie_krat($gebruiker_id, $mutatie_krat_aantal){
	global $pdo;

	//Voeg een mutatie toe aan de mutatie_krat tabel
	$stmt = $pdo->prepare("INSERT INTO mutatie_krat (gebruiker_id, mutatie) VALUES (:gebruiker_id, :mutatie_krat_aantal)");
	$stmt->bindParam(':gebruiker_id', $gebruiker_id);
	$stmt->bindParam(':mutatie_krat_aantal', $mutatie_krat_aantal);
	$stmt->execute();

	//Haal de huidige kratten stand op van de gebruiker
	$stmt = $pdo->prepare("SELECT aantal_krat FROM stand_krat WHERE gebruiker_id = :gebruiker_id");
	$stmt->bindParam(':gebruiker_id', $gebruiker_id);
	$stmt->execute();
	$huidige_stand = $stmt->fetch();
	$huidige_stand = $huidige_stand[0];

	echo $huidige_stand;

	//Schrijf de nieuwe kratten stand weg
	$nieuwe_stand = $huidige_stand + $mutatie_krat_aantal;
	$stmt = $pdo->prepare("UPDATE stand_krat SET aantal_krat = :nieuwe_stand WHERE gebruiker_id = :gebruiker_id");
	$stmt->bindParam(':nieuwe_stand', $nieuwe_stand);
	$stmt->bindParam(':gebruiker_id', $gebruiker_id);
	$stmt->execute();
}

function sorteer_op_fles($stand_fles){
	foreach ($stand_fles as $key => $row)
	{
	    $stand[$key] = $row['aantal_fles'];
	}
	$stand_fles = array_multisort($stand, SORT_DESC, $stand_fles);
}

//Deel voor statistieken

function geschiedenis_ophalen(){
	global $pdo;
	$stmt = $pdo->prepare('SELECT * FROM  `mutatie_fles` WHERE mutatie !=0 ORDER BY TIMESTAMP DESC LIMIT 100');
	$stmt->execute();
	$geschiedenis = $stmt->fetchAll(PDO::FETCH_ASSOC);
	return $geschiedenis;
}

function week_totalen($aantal = 10){
	global $pdo;
	$week_end = date('Y-m-d', strtotime('previous Sunday', time()));
	for ($i=0; $i < $aantal ; $i++) { 
		$stmt = $pdo->prepare('SELECT mutatie FROM  mutatie_fles WHERE (mutatie > 0 AND timestamp >= DATE_SUB(:date, INTERVAL 7 DAY) AND timestamp < :date)');
		$stmt->bindParam(':date', $week_end);
		$stmt->bindParam(':date', $week_end);
		$stmt->execute();
		$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
		foreach($result as $k=>$v) {
 		   $output[$k] = $v['mutatie'];
		}
		$week_totalen[] = array_sum($output);
		$week_nummers[] = date('W', strtotime($week_end));
		$week_end = date('Y-m-d', strtotime($week_end.'-7 days'));
	}
	return [$week_totalen, $week_nummers];

}

?>