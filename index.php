<?php
include('config.php');
include('functies.php');

$gebruikers = gebruikers_ophalen();
$geschiedenis = geschiedenis_ophalen();
if(sizeof($gebruikers) > 0){
	$stand_fles = aantal_fles_ophalen();	
}

$grafiek_data = week_totalen(20);
$week_nummers = $grafiek_data[1];
$grafiek_data = $grafiek_data[0];
?>



<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8"/>
		<title>Bierlijst by Corry!</title>
		<!-- mobile viewport optimisation -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<META HTTP-EQUIV="refresh" CONTENT="600">
		<!-- stylesheets -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
		<link rel="stylesheet" href="css/style.css" type="text/css"/>
		<script src="jquery-1.11.3.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
		<script src="Chart.js"></script>
		<script type="text/javascript">
			function lampjes(){
				$.get('http://192.168.178.99/webhook.php');
			}
		</script>
		<style>
			.panel-heading{
				padding-top: 5px;
				padding-bottom: 5px;
			}
		</style>
	</head>
	<body style="padding-top: 15px;">
		<div class="container">
			<div class="row">
				<div class="col-sm-4">
					<div class="panel panel-default">
						<div class="panel-heading">Biertjes doorvoeren</div>
						<div style="padding: 15px;">
							<form name="fles_mutatie" action="verwerk.php" method="post">
								<input type="hidden" name="fles_mutatie" value="1">

							<?php
							foreach($gebruikers as $gebruiker){
							?>
								<div class="row">
									<div class="col-xs-3">
										<label for"flesInvoer<?=$gebruiker['gebruiker_id']?>"><?=htmlspecialchars($gebruiker['gebruiker_naam'])?></label>
									</div>
									<div class="col-xs-7">
										<input oninput="amount<?=$gebruiker['gebruiker_id']?>.value=flesInvoer<?=$gebruiker['gebruiker_id']?>.value" type="range" id="flesInvoer<?=$gebruiker['gebruiker_id']?>" Value="0" name="flesInvoer<?=$gebruiker['gebruiker_id']?>" min="0" max="10">
									</div>
									<div class="col-xs-2">
										<output style="padding-bottom: 4px; padding-top: 2px;" name="amount<?=$gebruiker['gebruiker_id']?>" for="flesInvoer<?=$gebruiker['gebruiker_id']?>">0</output>
									</div>
								</div>
							<?php
							}
							?>
								<button type="submit" class="btn btn-success">Doervoeren!</button>
							</form>
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="panel panel-default">
						<div class="panel-heading">Kratjes doorvoeren</div>
						<div style="padding: 15px;">
							<form name="krat_mutatie" action="verwerk.php" method="post">
								<input type="hidden" name="krat_mutatie" value="1">

							<?php
							foreach($gebruikers as $gebruiker){
							?>
								<div class="row">
									<div class="col-xs-3">
										<label for"kratInvoer<?=$gebruiker['gebruiker_id']?>"><?=htmlspecialchars($gebruiker['gebruiker_naam'])?></label>
									</div>
									<div class="col-xs-7">
										<input oninput="amount<?=$gebruiker['gebruiker_id']?>.value=kratInvoer<?=$gebruiker['gebruiker_id']?>.value" type="range" id="kratInvoer<?=$gebruiker['gebruiker_id']?>" Value="0" name="kratInvoer<?=$gebruiker['gebruiker_id']?>" min="0" max="5">
									</div>
									<div class="col-xs-2">
										<output style="padding-bottom: 4px; padding-top: 2px;" name="amount<?=$gebruiker['gebruiker_id']?>" for="kratInvoer<?=$gebruiker['gebruiker_id']?>">0</output>
									</div>
								</div>
							<?php
							}
							?>
								<button type="submit" class="btn btn-success">Doervoeren!</button>
							</form>
					</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="panel panel-default">
						<div class="panel-heading">Instellingen</div>
						<div style="padding: 15px;">
							<div class="dropdown">
								<button class="btn btn-default btn-block dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
									Gebruiker toevoegen
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
									<li style="padding: 6px;">
										<form name="gebruiker_toevoegen" action="gebruiker_toevoegen.php" method="POST">
											<div class="col-lg-12">
												<div class="input-group">
													<input type="text" class="form-control" name="gebruiker_naam_toevoegen" id="gebruiker_naam_toevoegen" width="100%">
													<span class="input-group-btn">
														<button class="btn btn-default" type="submit"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></button>
													</span>
												</div>
											</div>
										</form>
									</li>
								</ul>
							</div>
							<br />
							<div class="dropdown">
								<button class="btn btn-default btn-block dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
									Gebruiker verwijderen
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
									<?php
									foreach($gebruikers as $gebruiker){
										echo '<li>';
											echo '<a href="gebruiker_verwijderen.php?id='.$gebruiker['gebruiker_id'].'">';
												echo htmlspecialchars($gebruiker['gebruiker_naam']);
											echo '<span class="glyphicon glyphicon-remove pull-right" aria-hidden="true"></a>';
										echo '</li>';
									}
									?>
								</ul>
							</div>
							<br />
							<button class="btn btn-default btn-block" type="button" onclick="lampjes();">
								Lampjes!
							</button>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-7">
					<div class="panel panel-default">
						<div class="panel-heading">Tussenstand</div>
						<div style="padding: 15px;">
							<table class="table table-striped table-condensed">
								<tr>
									<td>Plaats</td>
									<td>Naam</td>
									<td>Gedronken</td>
									<td>Gekocht</td>
									<td>Reserve</td>
								</tr>
							<?php
								if(sizeof($gebruikers) > 0){
									$stand_fles_totaal = 0;
									$stand_gekocht_totaal = 0;
									foreach($stand_fles as $key => $gebruiker){
										$gebruiker_naam_key = htmlspecialchars(gebruiker_naam_ophalen($stand_fles[$key]['gebruiker_id']));
										$stand_fles_key = $stand_fles[$key]['aantal_fles'];
										$stand_fles_totaal += $stand_fles_key;
										$stand_gekocht_key = aantal_krat_ophalen($stand_fles[$key]['gebruiker_id'])*24;
										$stand_gekocht_totaal += $stand_gekocht_key;
									?>
										<tr>
											<td><?=$key+1?></td>
											<td><?=$gebruiker_naam_key?></td>
											<td><?=$stand_fles_key?></td>
											<td><?=$stand_gekocht_key?></td>
											<td><?=$stand_gekocht_key-$stand_fles_key?></td>
										</tr>
									<?php
									}
									?>
										<tr>
											<td></td>
											<td>Totaal</td>
											<td><?=$stand_fles_totaal?></td>
											<td><?=$stand_gekocht_totaal?></td>
											<td><?=$stand_gekocht_totaal-$stand_fles_totaal?></td>
										</tr>
								<?php
								}
								?>
							</table>
						</div>
					</div>
				</div>
				<div class="col-sm-5">
					<div class="panel panel-default">
						<div class="panel-heading">Geschiedenis</div>
						<div class="pre-scrollable" style="padding: 15px; max-height: 298px;">
							<table class="table table-striped table-condensed">
								<tr>
									<td>Naam</td>
									<td>Aantal</td>
									<td>Datum</td>
								</tr>
								<?php
								foreach ($geschiedenis as $entry) {
									echo $entry['mutatie'] >= 5 ? '<tr class="success">' : '<tr>';
										echo '<td>'.gebruiker_naam_ophalen($entry['gebruiker_id']).'</td>';
										echo '<td>'.$entry['mutatie'].'</td>';
										echo '<td>'.date('j M - G:i', strtotime($entry['timestamp'])).'</td>';
									echo '</tr>';
								}
								?>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<div class="panel panel-default" style="padding-bottom: 5px;">
						<div class="panel-heading">Grafiek</div>
						<div style="padding: 15px;">
							<canvas id="canvas" width="100%" height="15"></canvas>
						</div>
						<script type="text/javascript">
		var data = {
			labels : [
				<?php
					foreach ($week_nummers as $key => $waarde) {
						if($key < count($week_nummers)){
							echo '"'.$waarde.'", ';
						}
						else{
							echo '"'.$waarde.'"';
						}
					}
				?>
					],
			datasets : [
				{
					label: "My First dataset",
					fillColor : "rgba(220,220,220,0.2)",
					strokeColor : "rgba(220,220,220,1)",
					pointColor : "rgba(220,220,220,1)",
					pointStrokeColor : "#fff",
					pointHighlightFill : "#fff",
					pointHighlightStroke : "rgba(220,220,220,1)",
					data : [
						<?php
							foreach ($grafiek_data as $key => $waarde) {
								if($key < count($grafiek_data)){
									echo $waarde.', ';
								}
								else{
									echo $waarde;
								}
							}
						?>
					]
				}
			]
		}
							window.onload = function(){
								var ctx = document.getElementById("canvas").getContext("2d");
								window.myBar = new Chart(ctx).Bar(data, {
									responsive : true
								});
							}
						</script>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
