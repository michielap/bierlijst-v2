<?php

include('config.php');
include('functies.php');

$gebruikers = gebruikers_ophalen();

$fles_mutatie = isset($_POST['fles_mutatie']) ? $_POST['fles_mutatie'] : false;
$krat_mutatie = isset($_POST['krat_mutatie']) ? $_POST['krat_mutatie'] : false;

if($fles_mutatie == 1){
	foreach($gebruikers as $gebruiker){
		mutatie_fles($gebruiker['gebruiker_id'], $_POST['flesInvoer'.$gebruiker['gebruiker_id']]);
	}
}
elseif($krat_mutatie == 1){
	foreach($gebruikers as $gebruiker){
		mutatie_krat($gebruiker['gebruiker_id'], $_POST['kratInvoer'.$gebruiker['gebruiker_id']]);
	}
}	

header("location: index.php");

?>